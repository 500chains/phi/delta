#!/usr/bin/env sh

##
## Input parameters
##
BINARY=/phid/${BINARY:-phid}
ID=${ID:-0}
LOG=${LOG:-phid.log}

##
## Assert linux binary
##
if ! [ -f "${BINARY}" ]; then
	echo "The binary $(basename "${BINARY}") cannot be found. Please add the binary to the shared folder. Please use
	the BINARY environment variable if the name of the binary is not 'phid' E.g.: -e BINARY=phid_my_test_version"
	exit 1
fi
BINARY_CHECK="$(file "$BINARY" | grep 'ELF 64-bit LSB executable, x86-64')"
if [ -z "${BINARY_CHECK}" ]; then
	echo "Binary needs to be OS linux, ARCH amd64"
	exit 1
fi

##
## Run binary with all parameters
##
export PHIDHOME="/phid/node${ID}/phid"

if [ -d "$(dirname "${PHIDHOME}"/"${LOG}")" ]; then
  "${BINARY}" --home "${PHIDHOME}" "$@" | tee "${PHIDHOME}/${LOG}"
else
  "${BINARY}" --home "${PHIDHOME}" "$@"
fi

