package ten

type KeySet struct {
	Name, Address, Pubkey, Brainkey string
}

var InitTen = []KeySet{
	{
		Name:    "init0",
		Address: "cosmos1w8e4xzxuq09qvpwmslc892ldc2yuhrn2rm4l39",
		Pubkey:  "cosmospub1addwnpepq0djwv20hhldfqt74k5nyfmz37cd28h4ppnr4unpqkhca0ws5lyevm845k4",
		Brainkey: `wool hundred often cry address length gadget mutual caution stomach good simple pride alarm life alien
			focus grit minor joke direct use opera one`,
	}, {
		Name:    "init1",
		Address: "cosmos1ckydc0e7vwucvetzznmn0ju83ew55cspgsq2ft",
		Pubkey:  "cosmospub1addwnpepq0sey09mfxuawz5a2qatl3hmm4qtye9ew0m9jy4tv4mdrtstwm7yunvmv7n",
		Brainkey: `torch tiny salon repeat upgrade choice mutual defense obscure disorder lucky output praise stumble
			trick adult manage cup diamond machine decrease pitch rate walnut`,
	}, {
		Name:    "init2",
		Address: "cosmos1kktlejz0kydhzrnmsqr79xsvmlmnlj3xpphknq",
		Pubkey:  "cosmospub1addwnpepqdve7tt0t47thwmqn4lm2asgyjkuv9vwfnukgajz5fdf9p8u7wykc00tccs",
		Brainkey: `scorpion reform mimic razor teach betray monkey barrel march save release second school manage
			start cash often pause skill school black off artist rice`,
	}, {
		Name:    "init3",
		Address: "cosmos1495vy9rr0y9rkedksy3qahws4seceexkhz842l",
		Pubkey:  "cosmospub1addwnpepq2yuj963ashxmtxmajmn6ysy4q350rm7k3tsq9pe4y0tk7qmcrtx5wdv49a",
		Brainkey: `attend toe number region excess match suspect exclude accident license recall attack spawn flavor
			shiver surround about shoulder shield yard wasp street uncover earn`,
	}, {
		Name:    "init4",
		Address: "cosmos12u0ucv4lpknf5yh90u3awd40calq3c83p5qg3c",
		Pubkey:  "cosmospub1addwnpepqwywa0zvpvlt3x7mferqhg8xt0d0nts032qhkrkkr8ssvd7zju9t6dzhhc4",
		Brainkey: `glance enact away pudding high fault deal find caught image food picture fan vault short mom peasant cliff
			volcano hybrid main chronic absurd fashion`,
	}, {
		Name:    "init5",
		Address: "cosmos1czcwh8qcxcgp6rdyrx9hmx6uajj362yjt5sndf",
		Pubkey:  "cosmospub1addwnpepq26us6pxn5x445z486zkfl7ug0v9azzzc344846fyntlrjcxwzxvue0sre6",
		Brainkey: `program time scheme occur stadium consider acquire survey maple income eager elegant space adapt where wine
			achieve satoshi dutch over cargo garment army void`,
	}, {
		Name:    "init6",
		Address: "cosmos1xcqqhezlpp0al3mc2fjnmgns7fvaqsuew8md5v",
		Pubkey:  "cosmospub1addwnpepqgkw5wfj30d99uhhx2a9jtxghxrzt3f5sjdzkyd2m38uk7kkawzn66edu3w",
		Brainkey: `detail betray tired heavy salute hamster disagree merge abstract day taxi disagree easy two avocado season
			primary few gather hood often animal fade peanut`,
	}, {
		Name:    "init7",
		Address: "cosmos17ksu4apwd8nh6e2vs0y397rkj8n0ns6xsdu6wp",
		Pubkey:  "cosmospub1addwnpepqgxek4gdunutw3yk7sajhgup7u0p465sty03023qmgurupqg873d7w0vkh9",
		Brainkey: `finger blade cabbage habit okay then market cry bird science spoon front keen outside era actor donor civil
			margin cram heavy connect runway season`,
	}, {
		Name:    "init8",
		Address: "cosmos1vc4cnnlcywr954rrz9qswfws6ktkfnwyfrxdrf",
		Pubkey:  "cosmospub1addwnpepqv7ec54d2acql6x4u2kue33p5dfnalf625qlv82s245gxe97tpnyc6u7sh0",
		Brainkey: `hurry range inflict robust cross average ribbon yard tag amateur speed estate quote zoo coyote scrap blossom
			surface display ginger best gossip level toss`,
	}, {
		Name:    "init9",
		Address: "cosmos106lku3u4l8ae0ehdhnkctexl09tdlny0zhr0qr",
		Pubkey:  "cosmospub1addwnpepqgc4pwduk78l4fthgst46my3auw0w98t0ymw0jtyq84gew6xa2w3w2h8jl7",
		Brainkey: `nominee wise crumble marble since creek surround oppose scissors ask average urge online outer language
			cradle prepare era genius garlic enact erode excite course`,
	},
}
