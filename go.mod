module gitlab.com/500chains/phi/delta

go 1.14

require (
	github.com/alfg/blockchain v0.0.0-20170304071410-09d957063c58
	github.com/cosmos/cosmos-sdk v0.34.4-0.20200511222341-80be50319ca5
	github.com/cosmos/launch v0.0.0-20191211160418-f0238e283980
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/otiai10/copy v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/snikch/goodman v0.0.0-20171125024755-10e37e294daa
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.6.1
	github.com/tendermint/go-amino v0.15.1
	github.com/tendermint/tendermint v0.33.4
	github.com/tendermint/tm-db v0.5.1
)
