# Simple usage with a mounted data directory:
# > docker build -t phi .
# > docker run -it -p 46657:46657 -p 46656:46656 -v ~/.phid:/phi/.phid -v ~/.phicli:/phi/.phicli phi phid init
# > docker run -it -p 46657:46657 -p 46656:46656 -v ~/.phid:/phi/.phid -v ~/.phicli:/phi/.phicli phi phid start
FROM golang:alpine AS build-env

# Set up dependencies
ENV PACKAGES curl make git libc-dev bash gcc linux-headers eudev-dev python3

# Set working directory for the build
WORKDIR /go/src/gitlab.com/500chains/phi/delta

# Add source files
COPY . .

# Install minimum necessary dependencies, build Cosmos SDK, remove packages
RUN apk add --no-cache $PACKAGES && \
    make install

# Final image
FROM alpine:edge

ENV PHI /phi

# Install ca-certificates
RUN apk add --update ca-certificates

RUN addgroup phiuser && \
    adduser -S -G phiuser phiuser -h "$PHI"
    
USER phiuser

WORKDIR $PHI

# Copy over binaries from the build-env
COPY --from=build-env /go/bin/phid /usr/bin/phid
COPY --from=build-env /go/bin/phicli /usr/bin/phicli

# Run phid by default, omit entrypoint to ease using container with phicli
CMD ["phid"]
